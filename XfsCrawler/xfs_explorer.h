//
// Created by pavel on 3/13/21.
//

#ifndef XFSCRAWLER_XFS_EXPLORER_H
#define XFSCRAWLER_XFS_EXPLORER_H

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <sys/stat.h>
#include <xfs/xfs.h>
#include <xfs/xfs_format.h>
#include <xfs/xfs_da_format.h>

typedef struct dir_inode_hdr {
    xfs_dinode_t *inode_core;
    size_t dir_count;
} dir_inode_hdr_t;

size_t root_inode_number;

//typedef struct xfs_dir3_sf_data_entry {
//    __be32 inumber;    /* inode number */
//    __u8 namelen;    /* name length */
//    __u8 filetype;    /* type of inode we point to */
//    __be16 tag;        /* starting offset of us */
//    char *name;        /* name bytes, no null */
//} xfs_dir3_sf_data_entry_t;

typedef struct xfs_dir3_blk_data_entry {
    __be64 inumber;    /* inode number */
    __u8 namelen;    /* name length */
    __u8 filetype;    /* type of inode we point to */
    __be16 tag;        /* starting offset of us */
    char *name;        /* name bytes, no null */
} xfs_dir3_blk_data_entry_t;

typedef struct us_file_content {
    char *content;
    size_t size;
} us_file_content_t;

xfs_sb_t parse_allocation_group(FILE *fs);

xfs_bmbt_irec_t *read_bmbt_record(FILE *fs);

xfs_dir3_blk_data_entry_t *read_dir3_sf_record(FILE *fs);

unsigned long long get_block_count(xfs_bmbt_rec_t *rec);

unsigned long long get_block_offset(xfs_bmbt_rec_t *rec);

unsigned long long get_start_block(xfs_bmbt_rec_t *rec);

xfs_dinode_t *read_inode_core(size_t inode_number, FILE *fs);

void le_be(void *ptr, size_t size);

void fread_sector_aligned(void *ptr, size_t size, size_t aligned_size, FILE *file);

size_t inode_to_offset(size_t inode_number);

xfs_dir3_blk_data_entry_t **read_dir_content_info(size_t inode_number, FILE *fs);

size_t fsblock_to_offset(size_t fsblock);

dir_inode_hdr_t *read_inode_dir_info(size_t inode_number, FILE *fs);

xfs_dinode_t *read_subdir_info(size_t inode, char *subdir_name, FILE *fs);

us_file_content_t read_file_contents(size_t inode_number, char *file_name, FILE *fs);

size_t read_inode_dir_header(size_t inode_number, FILE *fs);

size_t read_inode_blk_dir_count(size_t inode_number, FILE *fs);

xfs_bmbt_irec_t **read_file_block_records(xfs_dinode_t *inode, FILE *fs);

xfs_dir3_blk_data_entry_t **read_inode_blk_dir_records(size_t inode_number, FILE *fs);

xfs_dir3_blk_data_entry_t *read_dir3_blk_record(FILE *fs);

#endif //XFSCRAWLER_XFS_EXPLORER_H
