//
// Created by pavel on 3/29/21.
//

#ifndef XFSCRAWLER_XFS_CLI_H
#define XFSCRAWLER_XFS_CLI_H

#include "xfs_explorer.h"
#include "helper_functions.h"


char *pwd(us_directory_t *current_dir);

bool read_fs_superstruct(char* file_path);

us_directory_t *cd_command(char *path, us_directory_t *current_dir, char* file_path);

void ls_command(char *path, us_directory_t *current_dir, char* file_path);

void cat_command(char *path, us_directory_t *current_dir, char* file_path);

us_directory_t *enter_directory(us_directory_t *current_dir, char *directory_name, FILE *fs);

us_directory_t *leave_current_directory(us_directory_t *current_dir);

us_directory_t *enter_root(char* file_path);

void cp_command(char *src_path, char *dest_path, us_directory_t *current_dir, char* file_path);

void parse_new_command(char* file_path);

char* process_info_mode();

#endif //XFSCRAWLER_XFS_CLI_H
