#!/usr/bin/env ruby
require 'ffi'

module XfsModule
	extend FFI::Library
	ffi_lib './cmake-build-debug/libxfs_lib.so'
	attach_function :read_fs_superstruct, [:pointer], :bool
	attach_function :cd_command, [:string, :pointer, :pointer], :pointer
	attach_function :ls_command, [:string, :pointer, :pointer], :void
	attach_function :cat_command, [:string, :pointer, :pointer], :void
	attach_function :enter_root, [:pointer], :pointer
	attach_function :cp_command, [:string, :string, :pointer, :pointer], :void
	attach_function :pwd, [:pointer], :string
	attach_function :parse_new_command, [:string], :void
	attach_function :process_info_mode, [], :string
end

if __FILE__ == $0
	if ARGV[0] == "--info"
		puts XfsModule.process_info_mode()
	elsif ARGV[0] == "--crawl"
		if ARGV[1].nil?
			throw "Please, provide file path"
		end
		@file_path = ARGV[1]
		XfsModule.parse_new_command(@file_path)
	end
end
