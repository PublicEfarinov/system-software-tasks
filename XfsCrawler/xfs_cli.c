//
// Created by pavel on 3/29/21.
//

#include <stdio.h>
#include <stdlib.h>
#include "xfs_cli.h"

void parse_new_command(char *file_path) {
    char *command = malloc(1000);

    if (read_fs_superstruct(file_path)) {
        us_directory_t *current_dir = enter_root(file_path);
        if (current_dir == NULL) {
            return;
        }

        while (strcmp(command, "exit") != 0) {
            char *pwd_path = pwd(current_dir);
            printf("xfs_crawler%s$\t", pwd_path);
            free(pwd_path);
            scanf("%s", command);
            printf("command: %s\n", command);
            if (!strcmp(command, "cd")) {
                char *directory = malloc(1024);
                scanf("%s", directory);
                current_dir = cd_command(directory, current_dir, file_path);
                free(directory);
            } else if (!strcmp(command, "pwd")) {
                pwd_path = pwd(current_dir);
                printf("%s\n", pwd_path);
                free(pwd_path);
            } else if (!strcmp(command, "ls")) {
                char directory[1024] = {};
                scanf("%[^\n]s", directory);
                ls_command(trim_whitespace(directory), current_dir, file_path);
            } else if (!strcmp(command, "cat")) {
                char *fs_file_path = malloc(1024);
                scanf("%s", fs_file_path);
                cat_command(fs_file_path, current_dir, file_path);
                free(fs_file_path);
            } else if (!strcmp(command, "cp")) {
                char *src_file_path = malloc(1024);
                char *dest_path = malloc(1024);
                scanf("%s %s", src_file_path, dest_path);
                cp_command(src_file_path, dest_path, current_dir, file_path);
                free(src_file_path);
                free(dest_path);
            }
        }
        free(command);
        free_dir_stack(current_dir);
    } else {
        printf("Can not work with this file. Exiting...\n");
    }
}

bool read_fs_superstruct(char *file_path) {
    FILE *fs = fopen(file_path, "r");
    if (fs == NULL) {
        printf("Unable to open the file.\n");
        return false;
    }

    xfs_sb_t super_block = parse_allocation_group(fs);

    printf("Superblock magic is correct: %s\n", super_block.sb_magicnum == XFS_SB_MAGIC ? "true" : "FALSE");
    if (super_block.sb_magicnum != XFS_SB_MAGIC) {
        fclose(fs);
        return false;
    }
    printf("Allocation groups: %d\n", super_block.sb_agcount);
    printf("AG size: %d\n", super_block.sb_agblocks);
    printf("Disk sector size: %hu\n", super_block.sb_sectsize);
    printf("FS block size: %u\n", super_block.sb_blocksize);

    fclose(fs);
    return true;
}

us_directory_t *enter_root(char *file_path) {
    FILE *fs = fopen(file_path, "r");
    if (fs == NULL) {
        printf("Unable to open the file.\n");
        return NULL;
    }

    us_directory_t *root = malloc(sizeof(us_directory_t));
    root->name = malloc(strlen("root") + 1);
    strcpy(root->name, "root");
    dir_inode_hdr_t *inode = read_inode_dir_info(root_inode_number, fs);
    if (inode == NULL) {
        printf("Root inode \"%zu\" has unsupported format.", root_inode_number);
        fclose(fs);
        return NULL;
    }
    root->inode_number = inode->inode_core->di_ino;
    root->size = inode->dir_count;
    root->parent = NULL;
    root->was_successful = true;
    free(inode->inode_core);
    free(inode);
    fclose(fs);
    return root;
}

us_directory_t *cd_command(char *path, us_directory_t *current_dir, char *file_path) {
    FILE *fs = fopen(file_path, "r");
    if (fs == NULL) {
        printf("Unable to open the file.\n");
        return current_dir;
    }
    us_directory_t *new_dir = directory_from_path(get_dir_stack_copy(current_dir), path, fs);
    if (!new_dir->was_successful) {
        free_dir_stack(new_dir);
        printf("Was unable to find dir \"%s\"\n", path);
        fclose(fs);
        return current_dir;
    }
    free_dir_stack(current_dir);
    fclose(fs);
    return new_dir;
}

void cp_command(char *src_path, char *dest_path, us_directory_t *current_dir, char *file_path) {
    FILE *fs = fopen(file_path, "r");
    if (fs == NULL) {
        printf("Unable to open the file.\n");
        return;
    }

    us_directory_t *old_dir = current_dir;
    char *child_node = strrchr(src_path, '/');
    bool copy_file = false;
    if (child_node != NULL) {
        char *directory = malloc(1000);
        strncpy(directory, src_path, strlen(src_path));
        printf("directory: %s\n", directory);
        us_directory_t *new_directory = directory_from_path(get_dir_stack_copy(current_dir), directory, fs);
        if (new_directory->was_successful) {
            current_dir = new_directory;
        } else {
            free_dir_stack(new_directory);
            copy_file = true;
            strncpy(directory, src_path, child_node - src_path);
            child_node++;
            new_directory = directory_from_path(get_dir_stack_copy(current_dir), directory, fs);
            if (new_directory->was_successful) {
                current_dir = new_directory;
            } else {
                printf("Was unable to find dir \"%s\"\n", directory);
                free_dir_stack(new_directory);
                free(directory);
                fclose(fs);
                return;
            }
        }
        free(directory);
    } else {
        char *directory = malloc(1000);
        strncpy(directory, src_path, strlen(src_path));
        us_directory_t *new_directory = directory_from_path(get_dir_stack_copy(current_dir), directory, fs);
        if (new_directory->was_successful) {
            current_dir = new_directory;
        } else {
            free_dir_stack(new_directory);
            copy_file = true;
            child_node = src_path;
        }
        free(directory);
    }
    if (copy_file) {
        printf("file: %s\n", child_node);
        cp_file(child_node, dest_path, current_dir, fs);
    } else {
        copy_all_directory_contents(dest_path, current_dir, fs);
    }
    if (current_dir != old_dir) {
        free_dir_stack(current_dir);
    }
    fclose(fs);
}


void cat_command(char *path, us_directory_t *current_dir, char *file_path) {
    FILE *fs = fopen(file_path, "r");
    if (fs == NULL) {
        printf("Unable to open the file.\n");
        return;
    }

    us_directory_t *old_dir = current_dir;
    char *file = strrchr(path, '/');
    if (file != NULL) {
        char *directory = malloc(1000);
        strncpy(directory, path, file - path);
        file++;
        printf("directory: %s\n", directory);
        us_directory_t *new_directory = directory_from_path(get_dir_stack_copy(current_dir), directory, fs);
        if (new_directory->was_successful) {
            current_dir = new_directory;
        } else {
            printf("Was unable to find dir \"%s\"\n", directory);
            fclose(fs);
            free(directory);
            free_dir_stack(new_directory);
            return;
        }
        free(directory);
    } else {
        file = path;
    }
    printf("file: %s\n", file);

    us_file_content_t content = read_file_contents(current_dir->inode_number, file, fs);

    if (content.content != NULL) {
        char *array = malloc(content.size + 1);
        stpncpy(array, content.content, content.size);
        array[content.size] = '\0';
        for (int i = 0; i < content.size; ++i) {
            printf("%c", array[i]);
        }
        printf("\n");
        free(content.content);
        free(array);
    } else {
        printf("Was unable to find file \"%s\"\n", path);
    }
    if (current_dir != old_dir) {
        free_dir_stack(current_dir);
    }
    fclose(fs);
}

void ls_command(char *path, us_directory_t *current_dir, char *file_path) {
    FILE *fs = fopen(file_path, "r");
    if (fs == NULL) {
        printf("Unable to open the file.\n");
        return;
    }

    us_directory_t *old_dir = current_dir;
    if (strlen(path) > 0) {
        us_directory_t *new_directory = directory_from_path(get_dir_stack_copy(current_dir), path, fs);
        if (new_directory->was_successful) {
            current_dir = new_directory;
        } else {
            printf("Was unable to find dir \"%s\"\n", path);
            free_dir_stack(new_directory);
            fclose(fs);
            return;
        }
    }
    xfs_dir3_blk_data_entry_t **array = read_dir_content_info(current_dir->inode_number, fs);
    if (array == NULL) {
        printf("Operation failed. Could not read dir \"%s\" info.", current_dir->name);
        free(current_dir);
        fclose(fs);
        return;
    }
    printf("size: %zu\n", current_dir->size);
    for (int i = 0; i < current_dir->size; ++i) {
        if (array[i]->filetype == 2) {
            printf("Directory\t|\t");
        } else if (array[i]->filetype == 1) {
            printf("File\t|\t");
        }
        printf("%s\n", array[i]->name);
        free(array[i]->name);
        free(array[i]);
    }
    free(array);
    if (current_dir != old_dir) {
        free_dir_stack(current_dir);
    }
    fclose(fs);
}

char *pwd(us_directory_t *current_dir) {
    char *path = malloc(1024);
    path[0] = '\0';
    while (current_dir != NULL) {
        size_t len = strlen(current_dir->name);
        char *rev_name = malloc(1000);
        for (int i = 0; i < len; ++i) {
            rev_name[i] = current_dir->name[len - i - 1];
        }
        rev_name[len] = '\0';
        strcat(path, rev_name);
        strcat(path, "/");
        current_dir = current_dir->parent;
        free(rev_name);
    }
    size_t len = strlen(path);
    for (int i = 0; i < len / 2; ++i) {
        char c = path[i];
        path[i] = path[len - i - 1];
        path[len - i - 1] = c;
    }
    return path;
}

char* process_info_mode() {
    FILE* cmd = popen("lsblk --output=PATH,FSSIZE,FSTYPE,MOUNTPOINT", "r");
    char* buffer = NULL;
    buffer = malloc (1024 * 10);
    if (buffer)
    {
        char* output = malloc(1024);
        while (fgets(output, 1024, cmd) != NULL) {
            strcat(buffer, output);
        }
        free(output);
    }
    pclose (cmd);

    return buffer;
}