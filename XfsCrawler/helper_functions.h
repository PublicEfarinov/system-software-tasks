//
// Created by pavel on 3/30/21.
//

#ifndef XFSCRAWLER_HELPER_FUNCTIONS_H
#define XFSCRAWLER_HELPER_FUNCTIONS_H

#include <ctype.h>
#include <string.h>
#include <stdio.h>
#include <stdbool.h>

typedef struct us_directory us_directory_t;
struct us_directory {
    size_t inode_number;
    char *name;
    us_directory_t *parent;
    size_t size;
    bool was_successful;
};

char *trim_whitespace(char *str);

void free_dir_stack(us_directory_t *current_dir);

us_directory_t *get_dir_stack_copy(us_directory_t *current_dir);

us_directory_t *directory_from_path(us_directory_t *current_dir, char *path, FILE *fs);

void cp_file(char *src_path, char *dest_path, us_directory_t *current_dir, FILE *fs);

void copy_all_directory_contents(char *dest_path, us_directory_t *current_dir, FILE *fs);

us_directory_t *leave_current_directory(us_directory_t *current_dir);

us_directory_t *enter_directory(us_directory_t *current_dir, char *directory_name, FILE *fs);

us_directory_t *get_dir_stack_root(us_directory_t *current_dir);

void free_dir_stack_till_root(us_directory_t *current_dir);

#endif //XFSCRAWLER_HELPER_FUNCTIONS_H
