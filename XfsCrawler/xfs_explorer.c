//
// Created by pavel on 3/13/21.
//
#include <string.h>
#include "xfs_explorer.h"

size_t ag_size;
size_t ino_size;
size_t fsblock_size;
size_t dirblock_size;
size_t sector_size;
size_t inode_addr_offset;
size_t fsblk_addr_offset;

xfs_sb_t parse_allocation_group(FILE *fs) {
    xfs_sb_t super_block;

    fread(&super_block, sizeof(xfs_sb_t), 1, fs);

    le_be(&super_block.sb_magicnum, sizeof(super_block.sb_magicnum));
    le_be(&super_block.sb_sectsize, sizeof(super_block.sb_sectsize));
    le_be(&super_block.sb_agcount, sizeof(super_block.sb_agcount));
    le_be(&super_block.sb_agblocks, sizeof(super_block.sb_agblocks));
    le_be(&super_block.sb_blocksize, sizeof(super_block.sb_blocksize));
    le_be(&super_block.sb_inodesize, sizeof(super_block.sb_inodesize));
    le_be(&super_block.sb_rootino, sizeof(super_block.sb_rootino));
    le_be(&super_block.sb_agblklog, sizeof(super_block.sb_agblklog));
    le_be(&super_block.sb_inopblog, sizeof(super_block.sb_inopblog));

    ag_size = super_block.sb_agblocks;
    ino_size = super_block.sb_inodesize;
    sector_size = super_block.sb_sectsize;
    fsblock_size = super_block.sb_blocksize;
    dirblock_size = fsblock_size * (1 << (super_block.sb_dirblklog));
    root_inode_number = super_block.sb_rootino;
    inode_addr_offset = super_block.sb_agblklog + super_block.sb_inopblog;
    fsblk_addr_offset = super_block.sb_agblklog;

    fseek(fs, sector_size, SEEK_SET);
    return super_block;
}

xfs_dinode_t *read_inode_core(size_t inode_number, FILE *fs) {
    xfs_dinode_t *disk_inode = malloc(sizeof(xfs_dinode_t));
    fseek(fs, inode_to_offset(inode_number), SEEK_SET);
    fread(disk_inode, sizeof(*disk_inode), 1, fs);
    le_be(&disk_inode->di_magic, sizeof(disk_inode->di_magic));

    bool is_magic_good = disk_inode->di_magic == XFS_DINODE_MAGIC;

    if (is_magic_good) {
        le_be(&disk_inode->di_format, sizeof(disk_inode->di_format));
        le_be(&disk_inode->di_mode, sizeof(disk_inode->di_mode));
        le_be(&disk_inode->di_size, sizeof(disk_inode->di_size));
        le_be(&disk_inode->di_ino, sizeof(disk_inode->di_ino));
        le_be(&disk_inode->di_nextents, sizeof(disk_inode->di_nextents));
    } else {
        free(disk_inode);
        return NULL;
    }
    return disk_inode;
}

xfs_bmbt_irec_t *read_bmbt_record(FILE *fs) {
    xfs_bmbt_rec_t *record = malloc(sizeof(xfs_bmbt_rec_t));
    xfs_bmbt_irec_t *irecord = malloc(sizeof(xfs_bmbt_irec_t));
    fread(record, sizeof(xfs_bmbt_rec_t), 1, fs);
    le_be(&record->l0, sizeof(record->l0));
    le_be(&record->l1, sizeof(record->l1));

    irecord->br_startblock = get_start_block(record);
    irecord->br_blockcount = get_block_count(record);
    irecord->br_startoff = get_block_offset(record);
    free(record);
    return irecord;
}

unsigned long long get_block_offset(xfs_bmbt_rec_t *rec) {
    uint64_t block_offset_mask = ((1ull << 54) - 1) << 9;
    return ((rec->l0 & block_offset_mask) >> 9);
}

unsigned long long get_start_block(xfs_bmbt_rec_t *rec) {
    uint64_t start_block_mask = ((1ull << 43) - 1) << 21;
    uint64_t start_block_mask_1 = ((1ull << 9) - 1);
    return ((rec->l1 & start_block_mask) >> 21) + (rec->l0 & start_block_mask_1 << 43);
}

unsigned long long get_block_count(xfs_bmbt_rec_t *rec) {
    uint64_t block_count_mask = (1ull << 21) - 1;
    return rec->l1 & block_count_mask;
}


xfs_dir3_blk_data_entry_t *read_dir3_blk_record(FILE *fs) {
    size_t start = ftell(fs);
    xfs_dir3_blk_data_entry_t *entry = malloc(sizeof(xfs_dir3_blk_data_entry_t));
    fread(&entry->inumber, sizeof(entry->inumber), 1, fs);
    fread(&entry->namelen, sizeof(entry->namelen), 1, fs);
    le_be(&entry->inumber, sizeof(entry->inumber));
    le_be(&entry->namelen, sizeof(entry->namelen));

    size_t entry_size = sizeof(entry->namelen) + sizeof(entry->inumber) + sizeof(char) * entry->namelen + 1 +
                        sizeof(entry->filetype) + sizeof(entry->tag);
    entry_size = (entry_size / 16 + ((entry_size % 16) ? 1 : 0)) * 16;

    entry->name = malloc(entry->namelen + 1);
    fread(entry->name, entry->namelen, 1, fs);
    entry->name[entry->namelen] = '\0';

    fread(&entry->filetype, sizeof(entry->filetype), 1, fs);
    le_be(&entry->filetype, sizeof(entry->filetype));

    fseek(fs, start + entry_size - sizeof(entry->tag), SEEK_SET);
    fread(&entry->tag, sizeof(entry->tag), 1, fs);
    le_be(&entry->tag, sizeof(entry->tag));

    fseek(fs, start + entry_size, SEEK_SET);

    return entry;
}

xfs_dir3_blk_data_entry_t *read_dir3_sf_record(FILE *fs) {
    xfs_dir2_data_entry_t dir2;
    fread(&dir2.namelen, sizeof(dir2.namelen), 1, fs);
    le_be(&dir2.namelen, sizeof(dir2.namelen));

    xfs_dir3_blk_data_entry_t *entry = malloc(sizeof(xfs_dir3_blk_data_entry_t));
//    entry->data_entry_len = entry_size;
    fread(&entry->tag, sizeof(entry->tag), 1, fs);
    le_be(&entry->tag, sizeof(entry->tag));

    entry->name = malloc(dir2.namelen + 1);
    fread(entry->name, dir2.namelen, 1, fs);
    entry->name[dir2.namelen] = '\0';

    fread(&entry->filetype, sizeof(entry->filetype), 1, fs);
    le_be(&entry->filetype, sizeof(entry->filetype));
    dir2.inumber = 0;
    fread(&dir2.inumber, sizeof(int32_t), 1, fs);
    le_be(&dir2.inumber, sizeof(int32_t));

    entry->inumber = dir2.inumber;
    entry->namelen = dir2.namelen;
    return entry;
}

void le_be(void *ptr, size_t size) {
    for (size_t i = 0; i < size / 2; i++) {
        uint8_t block1 = ((uint8_t *) ptr)[i];
        uint8_t block2 = ((uint8_t *) ptr)[size - i - 1];
        ((uint8_t *) ptr)[i] = block2;
        ((uint8_t *) ptr)[size - i - 1] = block1;
    }
}

void le_fread(void *ptr, size_t size, size_t n, FILE *file) {
    fread(ptr, size, n, file);
    size_t buffer_size = size * n;
    for (size_t i = 0; i < buffer_size / 2; i++) {
        uint8_t block1 = ((uint8_t *) ptr)[i];
        uint8_t block2 = ((uint8_t *) ptr)[buffer_size - i - 1];
        ((uint8_t *) ptr)[i] = block2;
        ((uint8_t *) ptr)[buffer_size - i - 1] = block1;
    }
}

void fread_sector_aligned(void *ptr, size_t size, size_t aligned_size, FILE *file) {
    fread(ptr, size, 1, file);
    fseek(file, aligned_size - size, SEEK_CUR);
}

size_t inode_to_offset(size_t inode_number) {
    size_t relative = inode_number & ((1ull << inode_addr_offset) - 1);
    size_t ag_number = inode_number >> inode_addr_offset;
    return ag_number * ag_size * fsblock_size + relative * ino_size;
}

size_t fsblock_to_offset(size_t fsblock) {
    size_t relative = fsblock & ((1ull << fsblk_addr_offset) - 1);
    size_t ag_number = fsblock >> fsblk_addr_offset;
    return (ag_number * ag_size + relative) * fsblock_size;
}

xfs_dinode_t *read_root_inode(FILE *fs) {
    xfs_dinode_t *result;
    result = read_inode_core(root_inode_number, fs);
    return result;
}

xfs_dir3_blk_data_entry_t **read_dir_content_info(size_t inode_number, FILE *fs) {
    xfs_dinode_t *inode = read_inode_core(inode_number, fs);
    if (S_ISDIR(inode->di_mode)) {
        if (inode->di_format == XFS_DINODE_FMT_LOCAL) {
            size_t dir_count = read_inode_dir_header(inode_number, fs);
            xfs_dir3_blk_data_entry_t **result_array = malloc(sizeof(xfs_dir3_blk_data_entry_t *) * dir_count);
            for (int i = 0; i < dir_count; ++i) {
                xfs_dir3_blk_data_entry_t *data = read_dir3_sf_record(fs);
                result_array[i] = data;
            }
            free(inode);
            return result_array;
        } else if (inode->di_format == XFS_DINODE_FMT_EXTENTS) {
            xfs_dir3_blk_data_entry_t **result_array = read_inode_blk_dir_records(inode_number, fs);
            free(inode);
            return result_array;
        }
    }
    free(inode);
    return NULL;
}

us_file_content_t read_file_contents(size_t inode_number, char *file_name, FILE *fs) {
    xfs_dinode_t *inode = read_inode_core(inode_number, fs);
    us_file_content_t result;
    result.content = NULL;
    size_t subdir_count = 0;

    if (inode->di_format == XFS_DINODE_FMT_LOCAL) {
        dir_inode_hdr_t *ino_hdr = read_inode_dir_info(inode_number, fs);
        if (ino_hdr == NULL) {
            free(inode);
            return result;
        }
        subdir_count = ino_hdr->dir_count;

        free(ino_hdr->inode_core);
        free(ino_hdr);
    } else if (inode->di_format == XFS_DINODE_FMT_EXTENTS) {
        subdir_count = read_inode_blk_dir_count(inode_number, fs);
    }

    xfs_dir3_blk_data_entry_t **array = read_dir_content_info(inode_number, fs);
    if (array == NULL) {
        free(inode);
        return result;
    }

    char *buffer = NULL;
    for (int i = 0; i < subdir_count; ++i) {
        if (array[i]->filetype == 1 && !strcmp(array[i]->name, file_name)) {
            xfs_dinode_t *file_inode = read_inode_core(array[i]->inumber, fs);
            xfs_bmbt_irec_t **block_records = read_file_block_records(file_inode, fs);
            size_t buffer_size = file_inode->di_size;
            buffer = malloc(buffer_size);
            result.content = buffer;
            result.size = buffer_size;
            size_t buffer_filled = 0;
            for (int j = 0; j < file_inode->di_nextents; j++) {
                fseek(fs, fsblock_to_offset(block_records[j]->br_startblock), SEEK_SET);
                fread(buffer + buffer_filled, MIN(fsblock_size * block_records[j]->br_blockcount, buffer_size), 1, fs);
                buffer_filled += MIN(fsblock_size * block_records[j]->br_blockcount, buffer_size);
            }
            for (int j = 0; j < file_inode->di_nextents; ++j) {
                free(block_records[j]);
            }
            free(block_records);
            free(file_inode);
        }
    }
    for (int i = 0; i < subdir_count; ++i) {
        free(array[i]->name);
        free(array[i]);
    }
    free(array);
    free(inode);

    return result;
}

xfs_dinode_t *read_subdir_info(size_t inode_number, char *subdir_name, FILE *fs) {
    xfs_dinode_t *inode = read_inode_core(inode_number, fs);
    xfs_dinode_t *result = NULL;
    if (S_ISDIR(inode->di_mode)) {
        if (inode->di_format == XFS_DINODE_FMT_LOCAL) {
            size_t dir_count = read_inode_dir_header(inode_number, fs);
            for (int i = 0; i < dir_count; ++i) {
                xfs_dir3_blk_data_entry_t *data = read_dir3_sf_record(fs);
                if (!strcmp(data->name, subdir_name) && data->filetype == 2) {
                    result = read_inode_core(data->inumber, fs);
                    free(inode);
                    free(data->name);
                    free(data);
                    return result;
                }
                free(data->name);
                free(data);
            }
        } else if (inode->di_format == XFS_DINODE_FMT_EXTENTS) {
            size_t dir_count = read_inode_blk_dir_count(inode_number, fs);
            xfs_dir3_blk_data_entry_t **array = read_inode_blk_dir_records(inode_number, fs);
            for (int i = 0; i < dir_count; ++i) {
                if (!strcmp(array[i]->name, subdir_name) && array[i]->filetype == 2) {
                    result = read_inode_core(array[i]->inumber, fs);
                    free(inode);
                    for (int j = 0; j < dir_count; ++j) {
                        free(array[j]->name);
                        free(array[j]);
                    }
                    free(array);
                    return result;
                }
            }
            for (int j = 0; j < dir_count; ++j) {
                free(array[j]->name);
                free(array[j]);
            }
            free(array);
        }
    }
    free(inode);
    return result;
}

size_t read_inode_dir_header(size_t inode_number, FILE *fs) {
    xfs_dir2_sf_hdr_t dir_header;
    // skipping one inode
    fseek(fs, inode_to_offset(inode_number) + sizeof(xfs_dinode_t), SEEK_SET);
    fread(&dir_header, sizeof(uint8_t) * 6, 1, fs);
    le_be(&dir_header.count, sizeof(dir_header.count));
    le_be(&dir_header.i8count, sizeof(dir_header.i8count));
    le_be(&dir_header.parent, sizeof(dir_header.parent));
    return dir_header.count;
}

dir_inode_hdr_t *read_inode_dir_info(size_t inode_number, FILE *fs) {
    dir_inode_hdr_t *result = malloc(sizeof(dir_inode_hdr_t));
    result->inode_core = read_inode_core(inode_number, fs);
    if (S_ISDIR(result->inode_core->di_mode)) {
        if (result->inode_core->di_format == XFS_DINODE_FMT_LOCAL) {
            result->dir_count = read_inode_dir_header(inode_number, fs);
            return result;
        } else if (result->inode_core->di_format == XFS_DINODE_FMT_EXTENTS) {
            result->dir_count = read_inode_blk_dir_count(inode_number, fs);
            return result;
        }
    }
    free(result->inode_core);
    free(result);
    return NULL;
}

size_t read_inode_blk_dir_count(size_t inode_number, FILE *fs) {
    // skipping one inode
    fseek(fs, inode_to_offset(inode_number) + sizeof(xfs_dinode_t), SEEK_SET);
    xfs_bmbt_irec_t *dir_blk_record = read_bmbt_record(fs);

    xfs_dir2_block_tail_t dir_tail;
    fseek(fs, fsblock_to_offset(dir_blk_record->br_startblock) + dirblock_size - sizeof(xfs_dir2_block_tail_t),
          SEEK_SET);
    fread(&dir_tail, sizeof(xfs_dir2_block_tail_t), 1, fs);
    le_be(&dir_tail.count, sizeof(dir_tail.count));
    le_be(&dir_tail.stale, sizeof(dir_tail.stale));

    fseek(fs, fsblock_to_offset(dir_blk_record->br_startblock) + 0x50, SEEK_SET);

    free(dir_blk_record);
    return dir_tail.count - dir_tail.stale;
}

xfs_dir3_blk_data_entry_t **read_inode_blk_dir_records(size_t inode_number, FILE *fs) {
    // skipping one inode
    fseek(fs, inode_to_offset(inode_number) + sizeof(xfs_dinode_t), SEEK_SET);
    xfs_bmbt_irec_t *dir_blk_record = read_bmbt_record(fs);
    xfs_dir2_block_tail_t dir_tail;

    fseek(fs, fsblock_to_offset(dir_blk_record->br_startblock) + dirblock_size - sizeof(xfs_dir2_block_tail_t),
          SEEK_SET);
    fread(&dir_tail, sizeof(xfs_dir2_block_tail_t), 1, fs);
    le_be(&dir_tail.count, sizeof(dir_tail.count));
    le_be(&dir_tail.stale, sizeof(dir_tail.stale));

    xfs_dir3_blk_data_entry_t **array = malloc(
            sizeof(xfs_dir3_blk_data_entry_t *) * (dir_tail.count - dir_tail.stale));

    fseek(fs, fsblock_to_offset(dir_blk_record->br_startblock) + 0x40, SEEK_SET);

    for (int i = 0; i < dir_tail.count - dir_tail.stale; ++i) {
        array[i] = read_dir3_blk_record(fs);
    }
    free(dir_blk_record);
    return array;
}

xfs_bmbt_irec_t **read_file_block_records(xfs_dinode_t *inode, FILE *fs) {
    xfs_bmbt_irec_t **array = malloc(sizeof(xfs_bmbt_irec_t *));
    fseek(fs, inode_to_offset(inode->di_ino) + sizeof(xfs_dinode_t), SEEK_SET);

    for (int i = 0; i < inode->di_nextents; i++) {
        array[i] = read_bmbt_record(fs);
    }
    return array;
}