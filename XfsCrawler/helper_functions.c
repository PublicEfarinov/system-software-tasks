//
// Created by pavel on 3/30/21.
//

#include "helper_functions.h"
#include "xfs_cli.h"

char *trim_whitespace(char *str) {
    char *end;

    // Trim leading space
    while (isspace((unsigned char) *str)) str++;

    if (*str == 0)  // All spaces?
        return str;

    // Trim trailing space
    end = str + strlen(str) - 1;
    while (end > str && isspace((unsigned char) *end)) end--;

    // Write new null terminator character
    end[1] = '\0';

    return str;
}

us_directory_t *directory_from_path(us_directory_t *current_dir, char *path, FILE *fs) {
    if (strchr(path, '/') == path) {
        us_directory_t *new_root = get_dir_stack_root(current_dir);
        if (new_root == NULL) {
            current_dir->was_successful = false;
            return current_dir;
        }
//        free_dir_stack_till_root(current_dir);
        current_dir = new_root;
    }
    char *token = strtok(path, "/");
    while (token != NULL) {
        if (!strcmp(token, "..")) {
            us_directory_t *new_dir = leave_current_directory(current_dir);
            if (new_dir != NULL) {
                current_dir = new_dir;
            } else {
                printf("\"%s\" is root. It has no parent.\n", current_dir->name);
                current_dir->was_successful = false;
                return current_dir;
            }
        } else if (strcmp(token, ".") != 0) {
            us_directory_t *new_dir = enter_directory(current_dir, token, fs);
            if (new_dir != NULL) {
                current_dir = new_dir;
            } else {
                current_dir->was_successful = false;
                return current_dir;
            }
        }
        token = strtok(NULL, "/");
    }
    return current_dir;
}

void free_dir_stack(us_directory_t *current_dir) {
    while (current_dir != NULL) {
//        printf("removing %s copy\n", current_dir->name);
        us_directory_t *parent = current_dir->parent;
        free(current_dir->name);
        free(current_dir);
        current_dir = parent;
    }
}

void free_dir_stack_till_root(us_directory_t *current_dir) {
    while (current_dir->parent != NULL) {
//        printf("removing %s copy\n", current_dir->name);
        us_directory_t *parent = current_dir->parent;
        free(current_dir->name);
        free(current_dir);
        current_dir = parent;
    }
}

us_directory_t *get_dir_stack_root(us_directory_t *current_dir)
{
    while (current_dir->parent != NULL) {
        us_directory_t *parent = current_dir->parent;
        free(current_dir->name);
        free(current_dir);
        current_dir = parent;
    }
    return current_dir;
}

us_directory_t *get_dir_stack_copy(us_directory_t *current_dir) {
    if (current_dir != NULL) {
        us_directory_t *copy = malloc(sizeof(us_directory_t));
        copy->name = malloc(strlen(current_dir->name) + 1);
        strcpy(copy->name, current_dir->name);
        copy->size = current_dir->size;
        copy->inode_number = current_dir->inode_number;
        copy->was_successful = current_dir->was_successful;
        copy->parent = get_dir_stack_copy(current_dir->parent);
        return copy;
    }
    return NULL;
}


void cp_file(char *src_path, char *dest_path, us_directory_t *current_dir, FILE *fs) {
    us_directory_t *old_dir = current_dir;
    char *child_node = strrchr(src_path, '/');
    if (child_node != NULL) {
        char *directory = malloc(1000);
        strncpy(directory, src_path, child_node - src_path);
        child_node++;
        us_directory_t *stack_copy = get_dir_stack_copy(current_dir);
        us_directory_t *new_directory = directory_from_path(stack_copy, directory, fs);
        if (new_directory->was_successful) {
            current_dir = new_directory;
        } else {
            printf("Was unable to find dir \"%s\"\n", directory);
            free_dir_stack(stack_copy);
            free(directory);
            return;
        }
        free(directory);
    } else {
        child_node = src_path;
    }
    char *new_dest = malloc(1024);
    strcpy(new_dest, dest_path);
    if (new_dest[strlen(new_dest) - 1] != '/') {
        strcat(new_dest, "/");
    }
    strcat(new_dest, child_node);

    us_file_content_t content = read_file_contents(current_dir->inode_number, child_node, fs);

    if (content.content != NULL) {
        FILE *output_file = fopen(new_dest, "w+");
        if (output_file == NULL) {
            printf("Was unable to open destination file \"%s\"\n", new_dest);
        } else {
            printf("copying %s -> %s\n", src_path, new_dest);
            fwrite(content.content, content.size, 1, output_file);
            fclose(output_file);
        }
        free(content.content);
    } else {
        printf("Was unable to find child_node \"%s\"\n", src_path);
    }
    free(new_dest);
    if (current_dir != old_dir) {
        free_dir_stack(current_dir);
    }
}

void copy_all_directory_contents(char *dest_path, us_directory_t *current_dir, FILE *fs) {
    printf("COPYING DIRECTORY \"%s\"\n", current_dir->name);
    char *new_dest = malloc(1024);
    strcpy(new_dest, dest_path);
    strcat(new_dest, "/");
    strcat(new_dest, current_dir->name);
    struct stat st = {0};
    if (stat(new_dest, &st) == -1) {
        mkdir(new_dest, 0777);
    }
    xfs_dir3_blk_data_entry_t **array = read_dir_content_info(current_dir->inode_number, fs);
    if (array == NULL) {
        printf("Copying failed. Could not read dir \"%s\" info.", current_dir->name);
        return;
    }
    for (int i = 0; i < current_dir->size; ++i) {
        if (array[i]->filetype == 2 && strcmp(array[i]->name, ".") != 0 && strcmp(array[i]->name, "..") != 0) {
            us_directory_t *temp_dir = enter_directory(current_dir, array[i]->name, fs);
            copy_all_directory_contents(new_dest, temp_dir, fs);
            free(temp_dir->name);
            free(temp_dir);
        } else if (array[i]->filetype == 1) {
            cp_file(array[i]->name, new_dest, current_dir, fs);
        }
        free(array[i]->name);
        free(array[i]);
    }
    free(new_dest);
    free(array);
}

us_directory_t *enter_directory(us_directory_t *current_dir, char *directory_name, FILE *fs) {
    xfs_dinode_t *inode = read_subdir_info(current_dir->inode_number, directory_name, fs);
    if (inode == NULL) {
        free(inode);
        return NULL;
    }
    us_directory_t *new_directory = malloc(sizeof(us_directory_t));
    if (inode->di_format == XFS_DINODE_FMT_LOCAL) {
        new_directory->size = read_inode_dir_header(inode->di_ino, fs);
    } else if (inode->di_format == XFS_DINODE_FMT_EXTENTS) {
        new_directory->size = read_inode_blk_dir_count(inode->di_ino, fs);
    }

    new_directory->inode_number = inode->di_ino;
    new_directory->parent = current_dir;
    new_directory->name = malloc(strlen(directory_name) + 1);
    new_directory->was_successful = true;
    strcpy(new_directory->name, directory_name);
    free(inode);
    return new_directory;
}

us_directory_t *leave_current_directory(us_directory_t *current_dir) {
    us_directory_t *parent = current_dir->parent;
    if (parent != NULL) {
        free(current_dir->name);
        free(current_dir);
        return parent;
    } else {
        return NULL;
    }
}