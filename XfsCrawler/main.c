#include <stdio.h>
#include <string.h>
#include "xfs_explorer.h"
#include "xfs_cli.h"

const char *INFO_MESSAGE = "Available options:\n\t--info - shows information about connected devices.\n\t--crawl - interactive tool for managing files of XFS.\n";
const char *WRONG_ARGUMENT_MESSAGE = "Wrong argument format";

enum PROGRAM_MODES {
    INFO,
    CRAWLER,
};

int main(int arguments_number, char **args) {
    int current_mode = -1;
    char *file_path = NULL;
    if (arguments_number == 2 || arguments_number == 3) {
        if (strcmp(args[1], "--info") == 0) {
            current_mode = INFO;
        } else if (strcmp(args[1], "--crawl") == 0) {
            current_mode = CRAWLER;
            file_path = malloc(1024);
            strcpy(file_path, args[2]);
        } else {
            printf("%s - \"%s\".\n%s", WRONG_ARGUMENT_MESSAGE,
                   args[1], INFO_MESSAGE);
        }

        if (current_mode == INFO) {
            char* output = process_info_mode();
            printf("%s\n", output);
            free(output);
        } else if (current_mode == CRAWLER) {
            parse_new_command(file_path);
            free(file_path);
        }

    } else {
        printf("%s", INFO_MESSAGE);
    }
    printf("Job is done!\n");
    return 0;
}
