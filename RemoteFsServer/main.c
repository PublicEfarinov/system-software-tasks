#include <string.h>
#include <bits/pthreadtypes.h>
#include <pthread.h>
#include "server/server.h"
#include "client/client.h"

int main(int argc, char *argv[]) {
    if (strcmp(argv[1], "--server") == 0) {
        server_dir = argv[2];
        launch_server();
    } else if (strcmp(argv[1], "--client") == 0) {
        launch_client();
    } else if (strcmp(argv[1], "--test") == 0) {
        pthread_t threads[1];
        for (int i = 0; i < 1; ++i) {
            pthread_create(threads + i, NULL, launch_test_client, NULL);
        }
        pthread_exit(NULL);
    }
    return 0;
}