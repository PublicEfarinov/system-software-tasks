//
// Created by pavel on 5/22/21.
//

#ifndef REMOTEFSSERVER_FS_STRUCTS_H
#define REMOTEFSSERVER_FS_STRUCTS_H

#include <stdbool.h>
#include <bits/stdint-uintn.h>
enum FILE_TYPE{
    TFILE,
    DIRECTORY,
};
typedef struct server_command {
    char *command;
    char *argv;
} server_command_t;

typedef struct server_file {
    uint32_t size;
    enum FILE_TYPE type;
    char *name;
} server_file_t;

server_file_t **get_dir_files(char* dir, size_t* size);

#endif //REMOTEFSSERVER_FS_STRUCTS_H
