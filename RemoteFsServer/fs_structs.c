//
// Created by pavel on 5/22/21.
//

#include <dirent.h>
#include <string.h>
#include <sys/stat.h>
#include <malloc.h>
#include "fs_structs.h"

server_file_t **get_dir_files(char *dir, size_t *size) {
    DIR *command_dirent = opendir(dir);
    if (command_dirent == NULL) {
        return NULL;
    }
    struct dirent *entry;
    *size = 0;

    server_file_t **files = malloc(256 * sizeof(server_file_t *));
    while ((entry = readdir(command_dirent)) != NULL) {
        char full_file_path[256] = {};
        strcat(full_file_path, dir);
        strcat(full_file_path, entry->d_name);

        struct stat filestat = {0};
        if (stat(full_file_path, &filestat)) {
            for (int i = 0; i < *size; ++i) {
                free(files[i]);
            }
            free(files);
            closedir(command_dirent);
            return NULL;
        }

        server_file_t *file = malloc(sizeof(server_file_t));
        file->name = malloc(strlen(entry->d_name) + 1);
        strcpy(file->name, entry->d_name);
        file->size = filestat.st_size;
        if (S_ISREG(filestat.st_mode)) {
            file->type = TFILE;
        } else if (S_ISDIR(filestat.st_mode)) {
            file->type = DIRECTORY;
        }
        files[*size] = file;
//        printf("%s | %d | %d -> %d\n", file->name, file->size, filestat.st_mode, file->type);
        (*size)++;
    }
    closedir(command_dirent);
    return files;
}
