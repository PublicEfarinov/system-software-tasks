//
// Created by pavel on 5/22/21.
//

#ifndef REMOTEFSSERVER_CLIENT_H
#define REMOTEFSSERVER_CLIENT_H

#include <stdbool.h>
#include "../fs_structs.h"
#include <sys/types.h>

uint client_id;

typedef struct get_file_command_args {
    char *path;
    char *filename;
    char *host_dirname;
} get_file_command_args_t;

void launch_client();

void *launch_test_client(void *);

int connect_to_server();

uint get_client_id(int socket);

bool set_client_id(int socket);

server_file_t **update_current_dir(int socket, size_t *size, char *path);

void exit_command(int socket);

void exit_all_command(int socket);

bool mkdir_command(int socket, char *path);

void send_command(int socket, server_command_t *command);

server_file_t *recv_file_info(int socket);

server_file_t **recv_file_infos(int socket, size_t *size);

void *update_catalogs_thread(void *);

void *ping_thread(void *);

#endif //REMOTEFSSERVER_CLIENT_H
