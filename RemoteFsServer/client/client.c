//
// Created by pavel on 5/22/21.
//
#include <pthread.h>
#include <sys/socket.h>
#include <stdio.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <unistd.h>
#include "../comms/tcp_packet.h"
#include "client_ui.h"
#include "file_exchange.h"
#include "load_thread_list.h"
#include "client.h"

int connect_to_server() {
    int sockfd = 0, n = 0;
    struct sockaddr_in serv_addr = {};

    if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("\n Error : Could not create socket \n");
        return 0;
    }
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(5000);

    if (inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0) {
        printf("\n inet_pton error occured\n");
        return 0;
    }

    if (connect(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
        printf("\n Error : Connect Failed \n");
        return 0;
    }
    return sockfd;
}

void launch_client() {
    int socket = connect_to_server();

    client_id = get_client_id(socket);

    pthread_mutex_init(&list_lock, NULL);
    pthread_mutex_init(&directory_lock, NULL);

    pthread_t update_catalogs;
    pthread_create(&update_catalogs, NULL, update_catalogs_thread, NULL);

    thread_item_t *first = malloc(sizeof(thread_item_t));
    first->next = NULL;
    first->thread_id = new_thread_id++;
    first->thread = NULL;

    init_terminal_window();
    main_ui_loop(socket, first);

    exit_all_command(socket);

    pthread_cancel(update_catalogs);

    kill_all(first);
    free(first);

    pthread_mutex_destroy(&list_lock);
    pthread_mutex_destroy(&directory_lock);

    pthread_exit(NULL);
}

void *launch_test_client(void *_) {
    int socket = connect_to_server();
    client_id = get_client_id(socket);

    pthread_mutex_init(&list_lock, NULL);
    pthread_mutex_init(&directory_lock, NULL);

    client_directory = malloc(1024);
    strcpy(client_directory, "/");

    pthread_t update_catalogs;
    pthread_create(&update_catalogs, NULL, update_catalogs_thread, NULL);
    new_thread_id = 0;
    thread_item_t *first = malloc(sizeof(thread_item_t));
    first->next = NULL;
    first->thread_id = new_thread_id++;
    first->thread = NULL;
    sleep(2);

    root_files = update_current_dir(socket, &directory_size, "/");
    char current_directory[1024] = {};
    getcwd(current_directory, sizeof(current_directory));
    strcat(current_directory, "/");
    add_thread(first, upload_dir_command_thread("/", "CMakeFiles",
                                                "/home/pavel/deepspace/University/Labs/3rd year/system-software-tasks/RemoteFsServer/cmake-build-debug/"));
//    add_thread(first, get_file_command_thread("/CMakeFiles/", "clion-environment.txt", current_directory));
    sleep(5);
    exit_all_command(socket);

    for (int i = 0; i < directory_size; ++i) {
        printf("%s\n", root_files[i]->name);
        free(root_files[i]->name);
        free(root_files[i]);
    }
    free(root_files);

    pthread_cancel(update_catalogs);

    kill_all(first);
    free(first);
    pthread_mutex_destroy(&list_lock);
    pthread_mutex_destroy(&directory_lock);
    pthread_exit(NULL);
}


void send_command(int socket, server_command_t *command) {
    send_data(socket, command->command, strlen(command->command) + 1, 0);
    send_data(socket, command->argv, strlen(command->argv) + 1, 0);
}

server_file_t **recv_file_infos(int socket, size_t *size) {
    tcp_packet_t *packet = recv_packet(socket);
    if (packet == NULL) {
        return NULL;
    } else if (packet->packet_code != 0) {
        free(packet->data);
        free(packet);
        return NULL;
    }
    *size = *((size_t *) packet->data);
    free(packet->data);
    free(packet);
    server_file_t **array = malloc(sizeof(server_file_t *) * (*size));
    for (int i = 0; i < *size; ++i) {
        array[i] = recv_file_info(socket);
        if (array[i] == NULL) {
            for (int j = 0; j < i; ++j) {
                free(array[j]->name);
                free(array[j]);
            }
            free(array);
            return NULL;
        }
    }
    return array;
}

server_file_t *recv_file_info(int socket) {
    server_file_t *result = malloc(sizeof(server_file_t));
    tcp_packet_t *packet = recv_packet(socket);
    if (packet == NULL) {
        free(result);
        return NULL;
    } else if (packet->packet_code != 0) {
        free(result);
        free(packet->data);
        free(packet);
        return NULL;
    }
    result->size = *((uint32_t *) packet->data);

    free(packet->data);
    free(packet);
    packet = recv_packet(socket);
    if (packet == NULL) {
        free(result);
        return NULL;
    } else if (packet->packet_code != 0) {
        free(result);
        free(packet->data);
        free(packet);
        return NULL;
    }
    result->type = *((enum FILE_TYPE *) packet->data);
    free(packet->data);
    free(packet);

    packet = recv_packet(socket);
    if (packet == NULL) {
        free(result);
        return NULL;
    } else if (packet->packet_code != 0) {
        free(result);
        free(packet->data);
        free(packet);
        return NULL;
    }
    result->name = packet->data;
    free(packet);
    return result;
}

server_file_t **update_current_dir(int socket, size_t *size, char *path) {
    server_command_t command;
    command.command = "ls";
    command.argv = path;
    send_command(socket, &command);
    return recv_file_infos(socket, size);
}

void exit_command(int socket) {
    server_command_t command;
    command.command = "exit";
    command.argv = "";
    send_command(socket, &command);
}

void exit_all_command(int socket) {
    server_command_t command;
    command.command = "exit_all";
    command.argv = "";
    send_command(socket, &command);
}

bool mkdir_command(int socket, char *path) {
    server_command_t command;
    command.command = "mkdir";
    command.argv = path;
    send_command(socket, &command);

    tcp_packet_t *packet = recv_packet(socket);
    if (packet == NULL) {
        set_err_msg("An error occurred while creating dir on server");
        return false;
    } else if (packet->packet_code != 0) {
        set_err_msg(packet->data);
        free(packet->data);
        free(packet);
        return false;
    }
    free(packet->data);
    free(packet);
    return true;
}

uint get_client_id(int socket) {
    server_command_t command;
    command.command = "new_client";
    command.argv = "";
    send_command(socket, &command);

    tcp_packet_t *packet = recv_packet(socket);
    if (packet == NULL) {
        return -1;
    }
    if (packet->packet_code != 0) {
        free(packet->data);
        free(packet);
        return -1;
    }
    uint result = *((uint *) packet->data);
    free(packet->data);
    free(packet);
    return result;
}

bool set_client_id(int socket) {
    char id[64];
    sprintf(&id[0], "%d", client_id);
    server_command_t command;
    command.command = "old_client";
    command.argv = id;
    send_command(socket, &command);

    tcp_packet_t *packet = recv_packet(socket);
    if (packet == NULL) {
        return false;
    }
    if (packet->packet_code != 0) {
        free(packet->data);
        free(packet);
        return false;
    }
    free(packet->data);
    free(packet);
    return true;
}

void *update_catalogs_thread(void *_) {
    int socket = connect_to_server();
    set_client_id(socket);
    server_command_t command;
    command.command = "update";
    command.argv = "";
    send_command(socket, &command);

    while (!is_socket_closed(socket)) {
        tcp_packet_t *path_packet = recv_packet(socket);
        if (path_packet == NULL) {
            continue;
        }
        if (path_packet->packet_code != 0) {
            free(path_packet->data);
            free(path_packet);
            continue;
        }
        if (strcmp(path_packet->data, client_directory) == 0) {
            pthread_mutex_lock(&directory_lock);
            if(root_files != NULL) {
                for (int i = 0; i < directory_size; ++i) {
                    free(root_files[i]->name);
                    free(root_files[i]);
                }
                free(root_files);
            }
            root_files = update_current_dir(socket, &directory_size, client_directory);
            pthread_mutex_unlock(&directory_lock);
        } else {
            send_data(socket, "NOLS", 5, 0);
        }
        printf("Update end\n");
        send_data(socket, "OK", 3, 0);
        free(path_packet->data);
        free(path_packet);
    }
    shutdown(socket, SHUT_RDWR);
    close(socket);
    pthread_exit(NULL);
}

void *ping_thread(void *_) {
    int socket = connect_to_server();
    set_client_id(socket);
    server_command_t command;
    command.command = "ping";
    command.argv = "";
    send_command(socket, &command);

    while (!is_socket_closed(socket)) {
        tcp_packet_t *packet = recv_packet(socket);
        if (packet == NULL) {
            continue;
        }
        if (packet->packet_code != 0) {
            free(packet->data);
            free(packet);
            continue;
        }
        if (strcmp(packet->data, "ping") == 0) {
            send_data(socket, "OK", 3, 0);
            send_command(socket, &command);
        } else {
            send_data(socket, "ERR", 4, 1);
        }
    }
    shutdown(socket, SHUT_RDWR);
    close(socket);
    pthread_exit(NULL);

}
