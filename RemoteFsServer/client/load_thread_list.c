//
// Created by pavel on 5/25/21.
//

#include <malloc.h>
#include <unistd.h>
#include "load_thread_list.h"

atomic_uint new_thread_id = 0;

void add_thread(thread_item_t *first, pthread_t thread) {
    pthread_mutex_lock(&list_lock);
    while (first->next != NULL) {
        first = first->next;
    }
    first->next = malloc(sizeof(thread_item_t));
    first->next->thread = thread;
    first->next->thread_id = new_thread_id++;
    first->next->next = NULL;
    pthread_mutex_unlock(&list_lock);
}


void remove_thread(thread_item_t *first, int thread_id) {
    pthread_mutex_lock(&list_lock);
    while (first != NULL && first->next->thread_id != thread_id) {
        first = first->next;
    }
    if (!first) {
        return;
    }
    first->next = first->next->next;
    free(first->next);
    first->next = NULL;
    pthread_mutex_unlock(&list_lock);
}

void kill_all(thread_item_t *first) {
    while (first != NULL) {
        thread_item_t *next = first->next;
        if (first->thread != NULL) {
            pthread_cancel(first->thread);
            free(first);
        }
        first = next;
    }
}

pthread_t *thread_killer_start(thread_item_t *first) {
    pthread_t *thread = malloc(sizeof(pthread_t));
    pthread_create(thread, NULL, thread_killer, first);
    return thread;
}

void *thread_killer(void *first_ptr) {
    thread_item_t *first = first_ptr;
    while (1) {
        pthread_mutex_lock(&list_lock);
        thread_item_t *current = first->next;
        pthread_mutex_unlock(&list_lock);

        while (current != NULL && current->next != NULL) {
            thread_item_t *new = current->next;
            pthread_join(current->thread, NULL);
            remove_thread(first, current->thread_id);
            current = new;
        }
        if (current == NULL) {
            sleep(2);
        }
    }
    pthread_exit(NULL);
}

void add_thread_with_id(thread_item_t *first, pthread_t thread, uint id) {
    pthread_mutex_lock(&list_lock);
    while (first->next != NULL) {
        first = first->next;
    }
    first->next = malloc(sizeof(thread_item_t));
    first->next->thread = thread;
    first->next->thread_id = id;
    first->next->next = NULL;
    pthread_mutex_unlock(&list_lock);
}

void kill_all_with_id(thread_item_t *first, uint id) {
    pthread_mutex_lock(&list_lock);
    thread_item_t *node = first;
    while (node->next != NULL) {
        if (node->next->thread_id == id) {
            pthread_cancel(node->next->thread);
            void* old = node->next;
            node->next = node->next->next;
            free(old);
        } else { node = node->next; }
    }
    pthread_mutex_unlock(&list_lock);

}