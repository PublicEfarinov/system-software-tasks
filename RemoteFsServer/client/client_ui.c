//
// Created by pavel on 5/23/21.
//

#include <curses.h>
#include <unistd.h>
#include <string.h>
#include <malloc.h>
#include <bits/types/time_t.h>
#include <time.h>
#include <stdatomic.h>
#include <bits/pthreadtypes.h>
#include "../fs_structs.h"
#include "file_exchange.h"
#include "client.h"
#include "client_ui.h"


#define DELAY 50000
#define ERROR_DELAY 3

void init_terminal_window() {
    initscr();
    noecho();
    cbreak();
    nodelay(stdscr, TRUE);
    keypad(stdscr, TRUE);
    curs_set(FALSE);
}

void main_ui_loop(int socket, thread_item_t *first) {
    pthread_mutex_lock(&directory_lock);
    root_files = update_current_dir(socket, &directory_size, "/");
    pthread_mutex_unlock(&directory_lock);
    size_t selected_row = 0;

    client_directory = malloc(1024);
    strcpy(client_directory, "/");

    int current_char = getch();
    set_err_msg("");
    while (current_char != 'q') {
        clear();
        pthread_mutex_lock(&directory_lock);
        process_err_msg();
        if (current_char == KEY_DOWN) {
            if (selected_row < directory_size - 1) {
                selected_row++;
            }
        } else if (current_char == KEY_UP) {
            if (selected_row > 0) {
                selected_row--;
            }
        } else if (current_char == ' ') {
            if (root_files[selected_row]->type == DIRECTORY) {
                if (strcmp(root_files[selected_row]->name, "..") == 0) {
                    if (strcmp(client_directory, "/") == 0) {
                        set_err_msg("This is already root! Cannot open parent directory.");
                    } else {
                        remove_last_dir(client_directory);
                    }
                } else if (strcmp(root_files[selected_row]->name, ".") != 0) {
                    strcat(client_directory, root_files[selected_row]->name);
                    strcat(client_directory, "/");
                }
                for (int i = 0; i < directory_size; ++i) {
                    free(root_files[i]->name);
                    free(root_files[i]);
                }
                free(root_files);
                root_files = update_current_dir(socket, &directory_size, client_directory);
                selected_row = 0;

            } else {
                set_err_msg("This is a file. Can't cd into it.");
            }
        } else if (current_char == 'd') {
            if (root_files[selected_row]->type == TFILE) {
                add_thread(first, get_file_command_thread(client_directory, root_files[selected_row]->name, "./"));
            } else {
                if (strcmp(root_files[selected_row]->name, "..") == 0) {
                    set_err_msg("Please, select nested directory");
                } else if (strcmp(root_files[selected_row]->name, ".") == 0) {
                    set_err_msg("Please, select nested directory");
                } else {
                    char dirname[1024];
                    strcat(dirname, root_files[selected_row]->name);
                    add_thread(first, get_dir_command_thread(client_directory, dirname, "./"));
                }
            }
        } else if (current_char == 'u') {
            pthread_mutex_unlock(&directory_lock);
            upload_ui_loop(socket, client_directory, first);
            pthread_mutex_lock(&directory_lock);
        }

        wprintw(stdscr, "%d", selected_row);
        wmove(stdscr, 0, 30);
        waddstr(stdscr, client_directory);
        draw_files(root_files, directory_size, selected_row);
        draw_help_message();
        draw_error_message();
        char buffer_dump[1024];
        getstr(buffer_dump);
        refresh();
        pthread_mutex_unlock(&directory_lock);
        usleep(DELAY);
        current_char = getch();
    }
    endwin();
}

void draw_files(server_file_t **files, size_t size, size_t selected) {
    if (files == NULL) {
        set_err_msg("Could not get data from server. Restart the program.");
        return;
    }
    int max_y, max_x;
    getmaxyx(stdscr, max_y, max_x);
    int start_y = (max_y - size) / 2;
    int start_x = (max_x - 80) / 4;
    for (int i = 0; i < size; ++i) {
        if (i == selected) {
            attron(A_STANDOUT);
        }
        int row = i + start_y;
        wmove(stdscr, row, start_x);
        waddstr(stdscr, files[i]->name);
        char *filetype;
        if (files[i]->type == TFILE) {
            filetype = "<FILE>";
        } else {
            filetype = "<DIRECTORY>";
        }
        wmove(stdscr, row, start_x + 40 - strlen(filetype));
        waddstr(stdscr, filetype);

        mvwaddch(stdscr, row, start_x + 40, ACS_VLINE);
        wprintw(stdscr, "%d", files[i]->size);
        mvwaddch(stdscr, row, start_x + 55, ACS_VLINE);
        //TODO
        waddstr(stdscr, "last modified date here");
        attroff(A_STANDOUT);
    }
}

void draw_help_message() {
    int max_y, max_x;
    getmaxyx(stdscr, max_y, max_x);
    int start_y = max_y / 2 + 5;
    int start_x = max_x - 80;
    wmove(stdscr, start_y, start_x);
    waddstr(stdscr, "space --enter a directory");
    wmove(stdscr, start_y + 1, start_x);
    waddstr(stdscr, "d --download any file/directory to your current directory");
    wmove(stdscr, start_y + 2, start_x);
    waddstr(stdscr, "u --upload any file/directory to the server");
    wmove(stdscr, start_y + 3, start_x);
    waddstr(stdscr, "q --exit program");
}

void remove_last_dir(char *path) {
    while (*path != '\0') { path++; }
    path--;
    while (*(path - 1) != '/') { path--; }
    *path = '\0';
}

void draw_error_message() {
    int max_y, max_x;
    getmaxyx(stdscr, max_y, max_x);
    int start_y = max_y / 2 - 5;
    int start_x = max_x - 80;

    attron(A_BOLD);
    wmove(stdscr, start_y, start_x);
    waddstr(stdscr, error_message);
    attroff(A_BOLD);
}

void set_err_msg(char *str) {
    free(error_message);
    error_message = malloc(strlen(str) + 1);
    strcpy(error_message, str);
}

void upload_ui_loop(int socket, char *remote_dir, thread_item_t *first) {
    char current_directory[1024] = {};
    getcwd(current_directory, sizeof(current_directory));
    strcat(current_directory, "/");

    size_t size;
    server_file_t **root_files = get_dir_files(current_directory, &size);

    if (root_files == NULL) {
        char *buff = malloc(1024);
        sprintf(buff, "Error occurred processing dir '%s'", current_directory);
        set_err_msg(buff);
        free(buff);
        return;
    }

    int current_char = getch();

    set_err_msg("");

    size_t selected_row = 0;
    while (current_char != 'q') {
        clear();
        process_err_msg();
        if (current_char == KEY_DOWN) {
            if (selected_row < size - 1) {
                selected_row++;
            }
        } else if (current_char == KEY_UP) {
            if (selected_row > 0) {
                selected_row--;
            }
        } else if (current_char == ' ') {
            if (root_files[selected_row]->type == DIRECTORY) {
                if (strcmp(root_files[selected_row]->name, "..") == 0) {
                    if (strcmp(current_directory, "/") == 0) {
                        set_err_msg("This is already root! Cannot open parent directory.");
                    } else {
                        remove_last_dir(current_directory);
                    }
                } else if (strcmp(root_files[selected_row]->name, ".") != 0) {
                    strcat(current_directory, root_files[selected_row]->name);
                    strcat(current_directory, "/");
                }

                for (int i = 0; i < size; ++i) {
                    free(root_files[i]->name);
                    free(root_files[i]);
                }
                free(root_files);
                root_files = get_dir_files(current_directory, &size);
                selected_row = 0;
            } else {
                set_err_msg("This is a file. Can't cd into it.");
            }
        } else if (current_char == 'u') {
            if (root_files[selected_row]->type == TFILE) {
                add_thread(first,
                           upload_file_command_thread(remote_dir, root_files[selected_row]->name, current_directory));
            } else {
                if (strcmp(root_files[selected_row]->name, "..") == 0) {
                    set_err_msg("Please, select nested directory");
                } else if (strcmp(root_files[selected_row]->name, ".") == 0) {
                    set_err_msg("Please, select nested directory");
                } else {
                    add_thread(first,
                               upload_dir_command_thread(remote_dir, root_files[selected_row]->name, current_directory));
                }
            }
        }

        wprintw(stdscr, "%d", selected_row);
        wmove(stdscr, 0, 30);
        waddstr(stdscr, current_directory);
        waddstr(stdscr, root_files[selected_row]->name);
        draw_files(root_files, size, selected_row);
        draw_upload_help_message();
        draw_error_message();
        refresh();
        char buffer_dump[1024];
        getstr(buffer_dump);
        usleep(DELAY);
        current_char = getch();
    }
}

void process_err_msg() {
    static time_t err_msg_start;
    time_t current_time;
    time(&current_time);
    if (error_message[0] != '\0') {
        if (current_time - err_msg_start > ERROR_DELAY) {
            set_err_msg("");
            time(&err_msg_start);
        }
    } else {
        time(&err_msg_start);
    }
}

void draw_upload_help_message() {
    int max_y, max_x;
    getmaxyx(stdscr, max_y, max_x);
    int start_y = max_y / 2 + 5;
    int start_x = max_x - 80;
    wmove(stdscr, start_y, start_x);
    waddstr(stdscr, "space --enter a directory");
    wmove(stdscr, start_y + 1, start_x);
    waddstr(stdscr, "u --upload any file/directory to the server");
    wmove(stdscr, start_y + 3, start_x);
    waddstr(stdscr, "q --exit upload mode");

}
