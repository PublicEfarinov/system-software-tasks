//
// Created by pavel on 5/24/21.
//

#include <malloc.h>
#include <bits/pthreadtypes.h>
#include <pthread.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>
#include "file_exchange.h"
#include "client.h"
#include "client_ui.h"
#include "../comms/tcp_packet.h"

void get_file_command(int socket, char *path, char *filename, char *host_path) {
    char file_path[256];
    strcpy(file_path, path);
    strcat(file_path, filename);

    server_command_t command;
    command.command = "download";
    command.argv = file_path;
    send_command(socket, &command);

    tcp_packet_t *packet = recv_packet(socket);
    if (packet == NULL) {
        set_err_msg("Could not receive the file!");
        return;
    } else if (packet->packet_code != 0) {
        set_err_msg("Could not receive the file!");
        free(packet->data);
        free(packet);
        return;
    }

    char host_filepath[256];
    strcpy(host_filepath, host_path);
    strcat(host_filepath, filename);

    FILE *file = fopen(host_filepath, "w+");
    if (file == NULL) {
        set_err_msg("Could not receive the file!");
        free(packet->data);
        free(packet);
        return;
    }
    fwrite(packet->data, packet->size, 1, file);
    fclose(file);
    free(packet->data);
    free(packet);
}

void get_dir_command(int socket, char *path, char *dirname, char *host_dirname) {
    char dir_path[256];
    strcpy(dir_path, path);
    strcat(dir_path, dirname);
    strcat(dir_path, "/");

    char host_dir[256];
    strcpy(host_dir, host_dirname);
    strcat(host_dir, dirname);

    struct stat st = {0};
    if (stat(host_dir, &st) != -1) {
        set_err_msg("Could not create directory.");
        return;
    }

    mkdir(host_dir, 0777);
    size_t size;
    server_file_t **dir_files = update_current_dir(socket, &size, dir_path);

    if (dir_files == NULL) {
        set_err_msg("Error of reading subdir info!");
        return;
    }

    strcat(host_dir, "/");
    for (int i = 0; i < size; ++i) {
        if (dir_files[i]->type == TFILE) {
            get_file_command(socket, dir_path, dir_files[i]->name, host_dir);
        } else if (strcmp(dir_files[i]->name, ".") != 0 && strcmp(dir_files[i]->name, "..") != 0) {
            get_dir_command(socket, dir_path, dir_files[i]->name, host_dir);
        }
        free(dir_files[i]->name);
        free(dir_files[i]);
    }
    free(dir_files);
}

void *get_file_command_args(void *args) {
    get_file_command_args_t *args1 = (get_file_command_args_t *) args;
    int socket = connect_to_server();
    set_client_id(socket);
    get_file_command(socket, args1->path, args1->filename, args1->host_dirname);
    free(args1->filename);
    free(args1->host_dirname);
    free(args1->path);
    free(args);
    exit_command(socket);
    close(socket);
    set_err_msg("File download ended");
    pthread_exit(NULL);
}

void *get_dir_command_args(void *args) {
    get_file_command_args_t *args1 = (get_file_command_args_t *) args;
    int socket = connect_to_server();
    set_client_id(socket);
    get_dir_command(socket, args1->path, args1->filename, args1->host_dirname);
    free(args1->filename);
    free(args1->host_dirname);
    free(args1->path);
    free(args);
    exit_command(socket);
    close(socket);
    set_err_msg("Dir download ended");
    pthread_exit(NULL);
}

pthread_t get_file_command_thread(char *path, char *filename, char *host_path) {
    pthread_t thread;
    get_file_command_args_t *args = malloc(sizeof(get_file_command_args_t));
    args->path = malloc(strlen(path) + 1);
    args->filename = malloc(strlen(filename) + 1);
    args->host_dirname = malloc(strlen(host_path) + 1);
    strcpy(args->path, path);
    strcpy(args->filename, filename);
    strcpy(args->host_dirname, host_path);
    pthread_create(&thread, NULL, get_file_command_args, (void *) args);
    return thread;
}

pthread_t get_dir_command_thread(char *path, char *dirname, char *host_dirname) {
    pthread_t thread;
    get_file_command_args_t *args = malloc(sizeof(get_file_command_args_t));
    args->path = malloc(strlen(path) + 1);
    args->filename = malloc(strlen(dirname) + 1);
    args->host_dirname = malloc(strlen(host_dirname) + 1);
    strcpy(args->path, path);
    strcpy(args->filename, dirname);
    strcpy(args->host_dirname, host_dirname);
    pthread_create(&thread, NULL, get_dir_command_args, (void *) args);
    return thread;
}

void upload_file_command(int socket, char *path, char *filename, char *host_path) {
    char file_path[256];
    strcpy(file_path, path);
    strcat(file_path, filename);

    server_command_t command;
    command.command = "upload";
    command.argv = file_path;
    send_command(socket, &command);

    tcp_packet_t *packet = recv_packet(socket);
    if (packet == NULL) {
        set_err_msg("Could not send the file!");
        return;
    } else if (packet->packet_code != 0) {
        set_err_msg("Could not send the file!");
        free(packet->data);
        free(packet);
        return;
    }
    free(packet->data);
    free(packet);

    char host_filepath[256];
    strcpy(host_filepath, host_path);
    strcat(host_filepath, filename);

    FILE *file = fopen(host_filepath, "r");
    if (file != NULL) {
        struct stat filestat;
        stat(host_filepath, &filestat);
        char *buffer = malloc(filestat.st_size);
        fread(buffer, filestat.st_size, 1, file);
        send_data(socket, buffer, filestat.st_size, 0);
        free(buffer);
        fclose(file);
    } else {
        char *msg = "Error occurred while uploading the file";
        send_data(socket, msg, strlen(msg) + 1, 1);
    }
}

void upload_dir_command(int socket, char *path, char *dirname, char *host_dirname) {
    char dir_path[256];
    strcpy(dir_path, path);
    strcat(dir_path, dirname);
    strcat(dir_path, "/");

    if (!mkdir_command(socket, dir_path)) {
        return;
    }

    char host_dir[512];
    strcpy(host_dir, host_dirname);
    strcat(host_dir, dirname);
    strcat(host_dir, "/");

    size_t size;
    server_file_t **dir_files = get_dir_files(host_dir, &size);

    if (dir_files == NULL) {
        set_err_msg("Error of reading subdir info!");
        return;
    }

    for (int i = 0; i < size; ++i) {
        if (dir_files[i]->type == TFILE) {
            upload_file_command(socket, dir_path, dir_files[i]->name, host_dir);
        } else if (strcmp(dir_files[i]->name, ".") != 0 && strcmp(dir_files[i]->name, "..") != 0) {
            upload_dir_command(socket, dir_path, dir_files[i]->name, host_dir);
        }
        free(dir_files[i]->name);
        free(dir_files[i]);
    }
    free(dir_files);
}

pthread_t upload_file_command_thread(char *path, char *filename, char *host_path) {
    pthread_t thread;
    get_file_command_args_t *args = malloc(sizeof(get_file_command_args_t));
    args->path = malloc(strlen(path) + 1);
    args->filename = malloc(strlen(filename) + 1);
    args->host_dirname = malloc(strlen(host_path) + 1);
    strcpy(args->path, path);
    strcpy(args->filename, filename);
    strcpy(args->host_dirname, host_path);
    pthread_create(&thread, NULL, upload_file_command_args, (void *) args);
    return thread;
}

pthread_t upload_dir_command_thread(char *path, char *dirname, char *host_dirname) {
    pthread_t thread;
    get_file_command_args_t *args = malloc(sizeof(get_file_command_args_t));
    args->path = malloc(strlen(path) + 1);
    args->filename = malloc(strlen(dirname) + 1);
    args->host_dirname = malloc(strlen(host_dirname) + 1);
    strcpy(args->path, path);
    strcpy(args->filename, dirname);
    strcpy(args->host_dirname, host_dirname);
    pthread_create(&thread, NULL, upload_dir_command_args, (void *) args);
    return thread;
}

void *upload_dir_command_args(void *args) {
    get_file_command_args_t *args1 = (get_file_command_args_t *) args;
    int socket = connect_to_server();
    set_client_id(socket);
    upload_dir_command(socket, args1->path, args1->filename, args1->host_dirname);
    free(args1->filename);
    free(args1->host_dirname);
    free(args1->path);
    free(args);
    exit_command(socket);
    close(socket);
    pthread_exit(NULL);
}

void *upload_file_command_args(void *args) {
    get_file_command_args_t *args1 = (get_file_command_args_t *) args;
    int socket = connect_to_server();
    set_client_id(socket);
    upload_file_command(socket, args1->path, args1->filename, args1->host_dirname);
    free(args1->filename);
    free(args1->host_dirname);
    free(args1->path);
    free(args);
    exit_command(socket);
    close(socket);
    pthread_exit(NULL);
}
