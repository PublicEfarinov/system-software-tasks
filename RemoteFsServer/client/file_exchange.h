//
// Created by pavel on 5/24/21.
//

#ifndef REMOTEFSSERVER_FILE_EXCHANGE_H
#define REMOTEFSSERVER_FILE_EXCHANGE_H

void get_file_command(int socket, char *path, char *filename, char *host_dirname);

void get_dir_command(int socket, char *path, char *dirname, char *host_dirname);

void *get_file_command_args(void *);

void *get_dir_command_args(void *);

pthread_t get_file_command_thread(char *path, char *filename, char *host_path);

pthread_t get_dir_command_thread(char *path, char *dirname, char *host_dirname);

void upload_file_command(int socket, char *path, char *filename, char *host_dirname);

void upload_dir_command(int socket, char *path, char *dirname, char *host_dirname);

void *upload_file_command_args(void *);

void *upload_dir_command_args(void *);

pthread_t upload_file_command_thread(char *path, char *filename, char *host_path);

pthread_t upload_dir_command_thread(char *path, char *dirname, char *host_dirname);

#endif //REMOTEFSSERVER_FILE_EXCHANGE_H
