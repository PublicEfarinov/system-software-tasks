//
// Created by pavel on 5/23/21.
//

#ifndef REMOTEFSSERVER_CLIENT_UI_H
#define REMOTEFSSERVER_CLIENT_UI_H

#include "../fs_structs.h"
#include "load_thread_list.h"

pthread_mutex_t directory_lock;
atomic_char *error_message;

server_file_t **root_files;
size_t directory_size;
atomic_char *client_directory;

void init_terminal_window();

void draw_files(server_file_t **root_files, size_t size, size_t selected);

void draw_help_message();

void draw_upload_help_message();

void draw_error_message();

void remove_last_dir(char *path);

void set_err_msg(char *);

void process_err_msg();

void main_ui_loop(int socket, thread_item_t *first);

void upload_ui_loop(int socket, char *remote_dir, thread_item_t *first);

bool is_thead_running(pthread_t *thread);

#endif //REMOTEFSSERVER_CLIENT_UI_H
