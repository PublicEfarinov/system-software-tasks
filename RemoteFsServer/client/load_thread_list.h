//
// Created by pavel on 5/25/21.
//

#ifndef REMOTEFSSERVER_LOAD_THREAD_LIST_H
#define REMOTEFSSERVER_LOAD_THREAD_LIST_H

#include <pthread.h>
#include <stdatomic.h>
#include <sys/types.h>

atomic_uint new_thread_id;
pthread_mutex_t list_lock;

typedef struct thread_item thread_item_t;
struct thread_item {
    pthread_t thread;
    uint thread_id;
    thread_item_t *next;
};

void add_thread(thread_item_t *first, pthread_t thread);

void add_thread_with_id(thread_item_t *first, pthread_t thread, uint id);

void remove_thread(thread_item_t *first, int thread_id);

void kill_all(thread_item_t *first);

void kill_all_with_id(thread_item_t *first, uint id);

pthread_t *thread_killer_start(thread_item_t *first);

void *thread_killer(void *first);

#endif //REMOTEFSSERVER_LOAD_THREAD_LIST_H
