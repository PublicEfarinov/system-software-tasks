//
// Created by pavel on 5/22/21.
//

#include <unistd.h>
#include <string.h>
#include <netinet/in.h>
#include <pthread.h>
#include "server.h"
#include "../comms/tcp_packet.h"
#include "commands.h"
#include "../client/load_thread_list.h"
#include "../client/client_ui.h"

#define CONNECTION_DELAY 1
#define COMMAND_DELAY 75000

atomic_uint new_client_id = 0;

void launch_server() {
    int listenfd = 0, connfd = 0;
    struct sockaddr_in serv_addr = {};

    listenfd = socket(AF_INET, SOCK_STREAM, 0);

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    serv_addr.sin_port = htons(5000);

    if (bind(listenfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) != 0) {
        printf("could not open server socket\n");
        return;
    }

    listen(listenfd, 10);

    list_node_t *null_node = malloc(sizeof(list_node_t));
    null_node->sockfd = NULL;
    null_node->client_id = -1;
    null_node->next = NULL;
    null_node->prev = NULL;

    pthread_t command_thread;
    pthread_create(&command_thread, NULL, get_new_commands_thread, (void *) null_node);

    while (1) {
        connfd = accept(listenfd, (struct sockaddr *) NULL, NULL);
        printf("new connection: %d\n", connfd);
        push_node(null_node, connfd);
        sleep(CONNECTION_DELAY);
    }
    pthread_cancel(command_thread);
    pthread_exit(NULL);
}

server_command_t *get_command_from_socket(int socket) {
    server_command_t *result = malloc(sizeof(server_command_t));
    tcp_packet_t *packet = recv_packet(socket);
    if (packet == NULL) {
        free(result);
        return NULL;
    } else if (packet->packet_code != 0) {
        free(result);
        free(packet->data);
        free(packet);
        return NULL;
    }
    result->command = packet->data;

    free(packet);
    packet = recv_packet(socket);
    if (packet == NULL) {
        free(result);
        return NULL;
    } else if (packet->packet_code != 0) {
        free(result);
        free(packet->data);
        free(packet);
        return NULL;
    }
    result->argv = packet->data;
    free(packet);
    return result;
}

void send_file_infos(int sockfd, server_file_t *file) {
    char err = 0;
    if (!send_data(sockfd, &file->size, sizeof(file->size), err)) {
        err = 1;
    }
    if (!send_data(sockfd, &file->type, sizeof(file->type), err)) {
        err = 1;
    }
    send_data(sockfd, file->name, strlen(file->name) + 1, err);
}

void *process_command(void *command_arg) {
    thread_command_arg_t *command_wrp = (thread_command_arg_t *) command_arg;
    printf("Got command: %s %s at socket %d\n", command_wrp->command->command, command_wrp->command->argv,
           command_wrp->node->sockfd);
    if (strcmp(command_wrp->command->command, "exit") == 0) {
        shutdown(command_wrp->node->sockfd, SHUT_RDWR);
        close(command_wrp->node->sockfd);
    } else if (strcmp(command_wrp->command->command, "exit_all") == 0) {
        int sock = command_wrp->node->sockfd;
        remove_all_with_id(command_wrp->node);
        kill_all_with_id(command_wrp->first, command_wrp->node->client_id);
        shutdown(sock, SHUT_RDWR);
        close(sock);
    } else if (strcmp(command_wrp->command->command, "ls") == 0) {
        ls_command(command_wrp->node->sockfd, command_wrp->command->argv);
    } else if (strcmp(command_wrp->command->command, "download") == 0) {
        download_command(command_wrp->node->sockfd, command_wrp->command->argv);
    } else if (strcmp(command_wrp->command->command, "upload") == 0) {
        upload_command(command_wrp->node->sockfd, command_wrp->command->argv);
        char *parent_dir = malloc(256);
        strcpy(parent_dir, command_wrp->command->argv);
        remove_last_dir(parent_dir);
        add_thread_with_id(command_wrp->first,
                           send_update_thread(command_wrp->node, parent_dir),
                           command_wrp->node->client_id);
    } else if (strcmp(command_wrp->command->command, "mkdir") == 0) {
        create_dir_command(command_wrp->node->sockfd, command_wrp->command->argv);
        char *parent_dir = malloc(256);
        strcpy(parent_dir, command_wrp->command->argv);
        parent_dir[strlen(parent_dir) - 1] = '\0';
        remove_last_dir(parent_dir);
        add_thread_with_id(command_wrp->first,
                           send_update_thread(command_wrp->node, parent_dir),
                           command_wrp->node->client_id);
    } else if (strcmp(command_wrp->command->command, "new_client") == 0) {
        command_wrp->node->client_id = generate_client_id_command(command_wrp->node->sockfd);
        pthread_mutex_lock(&list_lock);
        thread_item_t *node = command_wrp->first;
        while (node->thread != pthread_self()) {
            node = node->next;
        }
        node->thread_id = command_wrp->node->client_id;
        pthread_mutex_unlock(&list_lock);
    } else if (strcmp(command_wrp->command->command, "old_client") == 0) {
        command_wrp->node->client_id = set_client_id_command(command_wrp->node->sockfd, command_wrp->command->argv);
    } else if (strcmp(command_wrp->command->command, "update") == 0) {
        pthread_mutex_init(&command_wrp->node->ping_lock, NULL);
        command_wrp->node->is_update = true;
    }
    printf("DONE\n");
    free(command_wrp->command);
    command_wrp->node->locked = false;
    free(command_wrp);
    pthread_exit(NULL);
}

_Noreturn void *get_new_commands_thread(void *null_node) {
    printf("started commands threads\n");
    list_node_t *root_node = null_node;

    new_thread_id = 0;
    thread_item_t *first = malloc(sizeof(thread_item_t));
    first->next = NULL;
    first->thread_id = -1;
    first->thread = NULL;

    while (1) {
        list_node_t *node = root_node->next;
//        print_list(root_node);
        while (node) {
            if (!node->locked && has_socket_data(node->sockfd)) {
                server_command_t *command = get_command_from_socket(node->sockfd);
                if (!command) {
                    printf("The node '%d' is dead. Removing...\n", node->client_id);
                    //TODO add locks, this is NOT threadsafe
                    list_node_t *next_node = node->next;
                    remove_node(node);
                    node = next_node;
                } else {
                    pthread_t complete_command_thread;
                    thread_command_arg_t *command_arg = malloc(sizeof(thread_command_arg_t));
                    command_arg->command = command;
                    command_arg->node = node;
                    command_arg->first = first;
                    node->locked = true;
                    pthread_create(&complete_command_thread, NULL, process_command, (void *) command_arg);
                    add_thread_with_id(first, complete_command_thread, node->client_id);
                    node = node->next;
                }
            } else if (is_socket_closed(node->sockfd)) {
                printf("The node '%d' is dead. Removing...\n", node->client_id);
                list_node_t *next_node = node->next;
                remove_node(node);
                node = next_node;
            } else {
                node = node->next;
            }
        }
        usleep(COMMAND_DELAY);
    }
    pthread_exit(NULL);
}

typedef struct update_args {
    list_node_t *first;
    char *path;
} update_args_t;

pthread_t send_update_thread(list_node_t *first, char *path) {
    update_args_t *args = malloc(sizeof(update_args_t));
    pthread_t update;
    args->first = first;
    args->path = path;
    pthread_create(&update, NULL, send_update, args);
    return update;
}

void *send_update(void *args) {

    list_node_t *first = ((update_args_t *) args)->first;
    char *path = ((update_args_t *) args)->path;
    while (first->prev) {
        first = first->prev;
    }

    while (first) {
        if (first->is_update) {
            if (!pthread_mutex_trylock(&first->ping_lock)) {
                printf("sending update at %d!\n", first->sockfd);
                send_data(first->sockfd, path, strlen(path) + 1, 0);
                tcp_packet_t *packet = recv_packet(first->sockfd);
                if (packet != NULL) {
                    if (strcmp(packet->data, "ls") == 0) {
                        recv_packet(first->sockfd);
                        ls_command(first->sockfd, path);
                    }
                    free(packet->data);
                    free(packet);
                    packet = recv_packet(first->sockfd);
                    free(packet->data);
                    free(packet);
                }
                pthread_mutex_unlock(&first->ping_lock);
            }
        }
        first = first->next;
    }
//    free(path);
    free(args);
    pthread_exit(NULL);
}
