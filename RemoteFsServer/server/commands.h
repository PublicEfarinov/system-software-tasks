//
// Created by pavel on 5/22/21.
//

#ifndef REMOTEFSSERVER_COMMANDS_H
#define REMOTEFSSERVER_COMMANDS_H

#include <sys/types.h>
#include "../comms/list_helper.h"
#include "../client/load_thread_list.h"

void ls_command(int sockfd, char *file_path);

void download_command(int sockfd, char *file_path);

void upload_command(int sockfd, char *file_path);

void create_dir_command(int sockfd, char *file_path);

uint set_client_id_command(int sockfd, const char *id);

uint generate_client_id_command(int sockfd);

void ping_command(thread_item_t *first, int sockfd, list_node_t *node);

#endif //REMOTEFSSERVER_COMMANDS_H
