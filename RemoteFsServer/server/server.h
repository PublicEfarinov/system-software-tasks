//
// Created by pavel on 5/22/21.
//

#ifndef REMOTEFSSERVER_SERVER_H
#define REMOTEFSSERVER_SERVER_H

#include "../fs_structs.h"
#include "../comms/list_helper.h"
#include "../client/load_thread_list.h"

char *server_dir;

atomic_uint new_client_id;

typedef struct thread_command_arg {
    server_command_t *command;
    list_node_t *node;
    thread_item_t *first;
} thread_command_arg_t;

_Noreturn void launch_server();

void *process_command(void *command_arg);

_Noreturn void *get_new_commands_thread(void *null_node);

server_command_t *get_command_from_socket(int socket);

void send_file_infos(int sockfd, server_file_t *file);

void *send_update(void *args);

pthread_t send_update_thread(list_node_t *first, char *path);

#endif //REMOTEFSSERVER_SERVER_H
