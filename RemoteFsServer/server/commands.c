//
// Created by pavel on 5/22/21.
//

#include <string.h>
#include <dirent.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include "commands.h"
#include "server.h"
#include "../comms/tcp_packet.h"
#include "../client/load_thread_list.h"

void ls_command(int sockfd, char *file_path) {
    char command_dir[256] = {};
    strcat(command_dir, server_dir);
    strcat(command_dir, file_path);

    size_t total_files;
    server_file_t **files = get_dir_files(command_dir, &total_files);

    send_data(sockfd, &total_files, sizeof(total_files), 0);
    for (int i = 0; i < total_files; ++i) {
        send_file_infos(sockfd, files[i]);
        free(files[i]);
    }
    free(files);
}

void download_command(int sockfd, char *file_path) {
    char command_dir[256] = {};
    strcat(command_dir, server_dir);
    strcat(command_dir, file_path);

    FILE *file = fopen(command_dir, "r");
    if (file != NULL) {
        struct stat filestat;
        stat(command_dir, &filestat);
        char *buffer = malloc(filestat.st_size);
        fread(buffer, filestat.st_size, 1, file);
        send_data(sockfd, buffer, filestat.st_size, 0);
        fclose(file);
    } else {
        char *msg = "Error occurred while sending the file";
        send_data(sockfd, msg, strlen(msg) + 1, 1);
    }
}

void upload_command(int sockfd, char *file_path) {
    char command_dir[256] = {};
    strcat(command_dir, server_dir);
    strcat(command_dir, file_path);

    FILE *file = fopen(command_dir, "w+");
    if (file == NULL) {
        char *msg = "Error occurred while creating the file";
        send_data(sockfd, msg, strlen(msg) + 1, 1);
        return;
    }
    send_data(sockfd, "", 1, 0);

    tcp_packet_t *packet = recv_packet(sockfd);
    if (packet == NULL) {
        printf("Could not receive the file!");
        fclose(file);
        return;
    } else if (packet->packet_code != 0) {
//        printf("%s\n", packet->data);
        free(packet->data);
        free(packet);
        fclose(file);
        return;
    }

    fwrite(packet->data, packet->size, 1, file);
    fclose(file);
    free(packet->data);
    free(packet);
}

void create_dir_command(int sockfd, char *file_path) {
    char command_dir[256] = {};
    strcat(command_dir, server_dir);
    strcat(command_dir, file_path);
    if (!mkdir(command_dir, 0777)) { send_data(sockfd, "", 1, 0); }
    else {
        char *msg = "Could not create directory";
        send_data(sockfd, msg, strlen(msg) + 1, 1);
    }
}

uint generate_client_id_command(int sockfd) {
    uint a = new_client_id++;
    send_data(sockfd, &a, sizeof(uint), 0);
    return a;
}

uint set_client_id_command(int sockfd, const char *id) {
    send_data(sockfd, "", 1, 0);
    return strtoull(id, NULL, 0);
}

void ping_command(thread_item_t *first, int sockfd, list_node_t *node) {
    if (!send_data(sockfd, "ping", 5, 0)) {
        printf("Connection with client %d died. Killing threads...", node->client_id);
        kill_all_with_id(first, node->client_id);
    }
    tcp_packet_t *packet = recv_packet(sockfd);

    sleep(1);
    if (packet == NULL) {
        return;
    }
    if (packet->packet_code != 0) {
        free(packet->data);
        free(packet);
    }
}
