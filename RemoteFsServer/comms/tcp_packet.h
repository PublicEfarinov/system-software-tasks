//
// Created by pavel on 5/22/21.
//

#ifndef REMOTEFSSERVER_TCP_PACKET_H
#define REMOTEFSSERVER_TCP_PACKET_H

#include <stddef.h>

#include <bits/stdint-uintn.h>
#include <stdbool.h>
#include <malloc.h>
#include <sys/socket.h>

typedef struct tcp_packet {
    uint32_t size;
    char packet_code;
    void *data;
} tcp_packet_t;

tcp_packet_t *recv_packet(int sockfd);

bool send_data(int sockfd, void *data, size_t size, char packet_code);

bool has_socket_data(int sockfd);

bool is_socket_closed(int sockfd);

#endif //REMOTEFSSERVER_TCP_PACKET_H
