//
// Created by pavel on 5/22/21.
//

#include <netinet/in.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include "tcp_packet.h"

tcp_packet_t *recv_packet(int sockfd) {
    tcp_packet_t *packet = malloc(sizeof(tcp_packet_t));

    if (recv(sockfd, &packet->size, sizeof(size_t), MSG_WAITALL) <= 0) {
        free(packet);
        return NULL;
    }
    packet->data = malloc(packet->size);

    if (recv(sockfd, &(packet->packet_code), 1, MSG_WAITALL) <= 0) {
        return packet;
    }

    if (packet->size == 0) {
        return packet;
    }

    if (recv(sockfd, packet->data, packet->size, MSG_WAITALL) < packet->size) {
        free(packet);
        return NULL;
    }
    return packet;
}

bool send_data(int sockfd, void *data, size_t size, char packet_code) {
    if (send(sockfd, &size, sizeof(size), MSG_NOSIGNAL) == -1) {
        printf("Could not send\n");
        return false;
    }

    if (send(sockfd, &packet_code, 1, MSG_NOSIGNAL) == -1) {
        printf("Could not send\n");
        return false;
    }

    if (send(sockfd, data, size, MSG_NOSIGNAL) == -1) {
        printf("Could not send\n");
        return false;
    }
    return true;
}

bool has_socket_data(int sockfd) {
//    printf("checking if the socket '%d' has any data...\t", sockfd);
    char _;
    if (recv(sockfd, &_, 1, MSG_PEEK | MSG_DONTWAIT) <= 0) {
//        printf("false\n");
        return false;
    }
//    printf("true\n");
    return true;
}

bool is_socket_closed(int sockfd) {
//    printf("checking if the socket '%d' is closed...\t", sockfd);
    int error = 0;
    socklen_t len = sizeof(error);
    int retval = getsockopt(sockfd, SOL_SOCKET, SO_ERROR, &error, &len);

    if (retval != 0) {
        /* there was a problem getting the error code */
        return true;
    }

    if (error != 0) {
        /* socket has a non zero error status */
        return true;
    }

    char _;
    if (recv(sockfd, &_, 1, MSG_PEEK | MSG_DONTWAIT) == -1) {
        return errno == ENOTCONN || errno == ECONNREFUSED;
    }
    return false;
}
