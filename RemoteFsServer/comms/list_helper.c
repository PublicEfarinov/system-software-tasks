//
// Created by pavel on 5/22/21.
//

#include <sys/socket.h>
#include <unistd.h>
#include "list_helper.h"

list_node_t *push_node(list_node_t *old, int val) {
    while (old->next) {
        old = old->next;
    }

    list_node_t *new_node = malloc(sizeof(list_node_t));
    new_node->sockfd = val;
    new_node->prev = old;
    new_node->locked = false;
    new_node->next = old->next;

    //left just in case
    if (old->next) {
        old->next->prev = new_node;
    }
    old->next = new_node;
    return new_node;
}

void remove_node(list_node_t *node) {
    printf("removed %d node\n", node->sockfd);
    if (node->next) {
        node->next->prev = node->prev;
    }
    if (node->prev) {
        node->prev->next = node->next;
    }
    free(node);
}

void remove_all_with_id(list_node_t *node) {
    uint id = node->client_id;
    while (node->prev) {
        node = node->prev;
    }
    while (node) {
        list_node_t *next = node->next;
        if (node->client_id == id) {
//            remove_node(node);
            shutdown(node->sockfd, SHUT_RDWR);
            close(node->sockfd);
        }
        node = next;
    }
}

void print_list(list_node_t *node) {
    while (node->prev) {
        node = node->prev;
    }

    while (node) {
        printf("%d | ", node->sockfd);
        node = node->next;
    }
    printf("\n");
}
