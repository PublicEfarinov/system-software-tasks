//
// Created by pavel on 5/22/21.
//

#ifndef REMOTEFSSERVER_LIST_HELPER_H
#define REMOTEFSSERVER_LIST_HELPER_H

#include <malloc.h>
#include <stdbool.h>
#include <stdatomic.h>
#include <sys/types.h>

typedef struct list_node list_node_t;
struct list_node {
    int sockfd;
    uint client_id;
    pthread_mutex_t ping_lock;
    bool is_update;
    atomic_bool locked;
    list_node_t *next;
    list_node_t *prev;
};

list_node_t *push_node(list_node_t *old, int val);

void remove_node(list_node_t *node);

void remove_all_with_id(list_node_t *node);

void print_list(list_node_t *node);

#endif //REMOTEFSSERVER_LIST_HELPER_H
